package com.example.famespadcharapon.viewpager;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class MyPagerAdapter extends FragmentPagerAdapter {
    private final int NUM_ITEMS = 3;

    public MyPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public int getCount() {
        return NUM_ITEMS;
    }

    public Fragment getItem(int position) {
        if(position == 0)
            return OneFragment.newInstance();
        else if(position == 1)
            return TwoFragment.newInstance();
        else if(position == 2)
            return ThreeFragment.newInstance();
        return null;
    }
}
